import {INSERT_EVENT, InsertEventCmd, InsertEventCmdRes} from '@chainizer/payment-api';
import {getHooks} from '@chainizer/support-app';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {getMongoose, Mongoose} from '@chainizer/support-mongoose';
import {expect} from 'chai';
import 'mocha';
import * as moment from 'moment';
import '../src/commands';
import {Payment} from '../src/Payment';

describe('insert-event', () => {
    let mongoose: Mongoose;

    before(async () => {
        await getHooks().startup[0]();
        mongoose = getMongoose();
        await Payment.remove({}).exec();

        const payment = new Payment({
            coin: 'coin',
            address: 'address',
            encryptedPrivateKey: 'encryptedPrivateKey',
            challenge: 'challenge',
            callbackUrl: 'callbackUrl'
        });
        await payment.save();
    });

    after(async () => {
        await getHooks().shutdown[0]();
    });

    it('should insert payment event', async () => {
        const payload: InsertEventCmd = {
            coin: 'coin',
            address: 'address',
            type: 'notification',
            attemptedAt: moment().toISOString(),
            successful: true
        };
        const metadata: Metadata = {
            challenge: 'challenge'
        };

        const r = <InsertEventCmdRes> await executeTask(TaskType.command, INSERT_EVENT, payload, metadata);
        expect(r).to.have.property('events');
        expect(r).to.have.nested.property('events[0].type', 'notification');
        expect(r).to.have.nested.property('events[0].attemptedAt');
        expect(r).to.have.nested.property('events[0].successful', true);
        expect(r.events).to.have.lengthOf(1);
    });

    it('should failed when bad challenge', async () => {
        const payload: InsertEventCmd = {
            coin: 'coin',
            address: 'address',
            type: 'notification',
            attemptedAt: moment().toISOString(),
            successful: true
        };
        const metadata: Metadata = {
            challenge: 'unknown_challenge'
        };

        try {
            const r = <InsertEventCmdRes> await executeTask(TaskType.command, INSERT_EVENT, payload, metadata);
            expect(r).to.be.null;
        } catch (e) {
            expect(e).to.have.property('name', 'PaymentNotFound');
            expect(e).to.have.property('message', 'Payment cannot be found.');
            expect(e).to.have.property('status', 404);

        }
    });

});

