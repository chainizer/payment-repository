import {expect} from 'chai';
import 'mocha';
import '../src/main';
import {getHooks, getServer} from '@chainizer/support-app';
import {config} from '@chainizer/support-config';
import {dispatchTask, getCnqAmqp} from '@chainizer/support-cnq';
import {TaskType} from '@chainizer/support-executor';
import {FETCH_PAYMENT, INSERT_EVENT, INSERT_PAYMENT} from '@chainizer/payment-api';

config().set('cnq.producer.amqp.command.repository', {
    exchange: 'payment.repository',
    routingKey: 'command'
});

config().set('cnq.producer.amqp.query.repository', {
    exchange: 'payment.repository',
    routingKey: 'query'
});

const wait = (ms) => (new Promise(resolve => setTimeout(resolve, ms)));

describe('app', () => {

    it('should check health and stop the app', async () => {
        await wait(100);

        let healthError;
        try {
            getHooks().health.forEach(async hook => await hook());
        } catch (e) {
            healthError = e;
        }

        const amqp = await getCnqAmqp();
        await amqp.channel.checkExchange('payment.repository');
        await amqp.channel.checkQueue('payment.repository.command_backlog');
        await amqp.channel.checkQueue('payment.repository.command_invalid');
        await amqp.channel.checkQueue('payment.repository.query_backlog');
        await amqp.channel.checkQueue('payment.repository.query_invalid');

        try {
            const r = await dispatchTask('repository', TaskType.command, INSERT_PAYMENT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        try {
            const r = await dispatchTask('repository', TaskType.command, INSERT_EVENT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        try {
            const r = await dispatchTask('repository', TaskType.query, FETCH_PAYMENT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        await wait(100);

        let shutdownError;
        try {
            getHooks().shutdown.forEach(async hook => await hook());
        } catch (e) {
            shutdownError = e;
        }

        getServer().close();
        expect(healthError, 'health hooks failed').to.not.exist;
        expect(shutdownError, 'shutdown hooks failed').to.not.exist;
    });

});

