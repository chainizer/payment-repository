import {FETCH_PAYMENT, FetchPaymentQry, FetchPaymentQryRes} from '@chainizer/payment-api';
import {getHooks} from '@chainizer/support-app';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {getMongoose, Mongoose} from '@chainizer/support-mongoose';
import {expect} from 'chai';
import 'mocha';
import {Payment} from '../src/Payment';
import '../src/queries';

describe('fetch-payment', () => {
    let mongoose: Mongoose;

    before(async () => {
        await getHooks().startup[0]();
        mongoose = getMongoose();
        await Payment.remove({}).exec();

        const payment = new Payment({
            coin: 'coin',
            address: 'address',
            encryptedPrivateKey: 'encryptedPrivateKey',
            challenge: 'challenge',
            callbackUrl: 'callbackUrl'
        });
        await payment.save();
    });

    after(async () => {
        await getHooks().shutdown[0]();
    });

    it('should fetch payment', async () => {
        const payload: FetchPaymentQry = {
            coin: 'coin',
            address: 'address'
        };
        const metadata: Metadata = {
            challenge: 'challenge'
        };

        const r = <FetchPaymentQryRes> await executeTask(TaskType.query, FETCH_PAYMENT, payload, metadata);
        expect(r).to.have.property('address', 'address');
        expect(r).to.have.property('encryptedPrivateKey', 'encryptedPrivateKey');
        expect(r).to.have.property('challenge', 'challenge');
        expect(r).to.have.property('callbackUrl', 'callbackUrl');
        expect(r).to.have.property('createdAt');
        expect(r).to.have.property('updatedAt');
    });

    it('should fetch payment', async () => {
        const payload: FetchPaymentQry = {
            coin: 'coin',
            address: 'address'
        };
        const metadata: Metadata = {
            challenge: 'challenge1'
        };
        try {
            const r = <FetchPaymentQryRes> await executeTask(TaskType.query, FETCH_PAYMENT, payload, metadata);
            expect(r).to.be.null;
        } catch (e) {
            expect(e).to.have.property('name', 'PaymentNotFound');
            expect(e).to.have.property('message', 'Payment cannot be found.');
            expect(e).to.have.property('status', 404);

        }
    });

});

