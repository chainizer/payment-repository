import {INSERT_PAYMENT, InsertPaymentCmd, InsertPaymentCmdRes} from '@chainizer/payment-api';
import {getHooks} from '@chainizer/support-app';
import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {getMongoose, Mongoose} from '@chainizer/support-mongoose';
import {expect} from 'chai';
import 'mocha';
import '../src/commands';
import {Payment} from '../src/Payment';

describe('insert-payment', () => {
    let mongoose: Mongoose;

    before(async () => {
        await getHooks().startup[0]();
        mongoose = getMongoose();
        await Payment.remove({}).exec();
    });

    after(async () => {
        await getHooks().shutdown[0]();
    });

    it('should insert payment', async () => {
        const payload: InsertPaymentCmd = {
            coin: 'coin',
            address: 'address',
            encryptedPrivateKey: 'encryptedPrivateKey',
            challenge: 'challenge',
            callbackUrl: 'callbackUrl'
        };
        const metadata: Metadata = {};
        const r = <InsertPaymentCmdRes> await executeTask(TaskType.command, INSERT_PAYMENT, payload, metadata);
        expect(r).to.have.property('coin', 'coin');
        expect(r).to.have.property('address', 'address');
        expect(r).to.have.property('encryptedPrivateKey', 'encryptedPrivateKey');
        expect(r).to.have.property('challenge', 'challenge');
        expect(r).to.have.property('callbackUrl', 'callbackUrl');
        expect(r).to.have.property('createdAt');
        expect(r).to.have.property('updatedAt');
    });

});

