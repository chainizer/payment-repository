#!/usr/bin/env bash

# MONGO

MONGO_URL="mongodb://"
if [ ! -z ${MONGO_USER} ]; then
    MONGO_URL="${MONGO_URL}${MONGO_USER}:${MONGO_PASS}@"
fi
IFS=',' read -ra hosts <<< "${MONGO_HOSTS:-mongo-0}";
for MONGO_HOST in "${hosts[@]}"
do
    MONGO_URL="${MONGO_URL}${MONGO_HOST},"
done
MONGO_URL=`echo ${MONGO_URL} | sed "s/,$//"`
MONGO_URL="${MONGO_URL}/payment?replicaSet=payment"

# RABBIT

RABBIT_URL="amqp://"
if [ ! -z ${RABBIT_PASS} ]; then
    RABBIT_URL="${RABBIT_URL}${RABBIT_USER}:${RABBIT_PASS}@"
fi
RABBIT_URL="${RABBIT_URL}${RABBIT_HOST:-rabbit-discovery}/"

# START

ARGS="$@ --set.mongoose.url ${MONGO_URL} --set.cnq.amqp.connection ${RABBIT_URL}"

exec npm start --production -- ${ARGS}
