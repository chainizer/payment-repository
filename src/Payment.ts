import {Document, Model, model, Schema} from 'mongoose';

export const EventSchema = new Schema({
    type: {type: String, required: true, enum: ['notification', 'withdraw']},
    attemptedAt: {type: Date, required: true},
    successful: {type: Boolean, required: true},
    error: {type: String, trim: true}
});

export const PaymentSchema = new Schema({
    coin: {type: String, required: true, trim: true, lowercase: true},

    address: {type: String, required: true, trim: true, lowercase: true, unique: true},
    encryptedPrivateKey: {type: String, required: true, trim: true},
    challenge: {type: String, required: true, trim: true, index: true},

    callbackUrl: {type: String, trim: true},

    events: [EventSchema]
}, {
    collection: 'payment',
    timestamps: true
});

PaymentSchema.index({
    createdAt: 1,
    lastNotificationAt: 1,
    lastWithdrawAt: 1
});

export const Payment: Model<Document> = model('Payment', PaymentSchema);
