import {Metadata, registerTask, TaskType} from '@chainizer/support-executor';
import {FETCH_PAYMENT, FetchPaymentQry, FetchPaymentQryRes, PaymentNotFound} from '@chainizer/payment-api';
import {Payment} from './Payment';

registerTask(TaskType.query, FETCH_PAYMENT, async (payload: FetchPaymentQry, metadata: Metadata): Promise<FetchPaymentQryRes> => {
    const payment = await Payment.findOne({
        coin: payload.coin,
        address: payload.address,
        challenge: metadata.challenge
    });
    if (!payment) {
        throw new PaymentNotFound();
    }
    return <FetchPaymentQryRes> payment.toObject();
});
