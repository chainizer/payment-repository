import {Metadata, registerTask, TaskType} from '@chainizer/support-executor';
import {omit} from 'lodash';
import {Payment} from './Payment';
import {
    INSERT_EVENT,
    INSERT_PAYMENT,
    InsertEventCmd,
    InsertEventCmdRes,
    InsertPaymentCmd,
    InsertPaymentCmdRes,
    PaymentNotFound
} from '@chainizer/payment-api';

registerTask(TaskType.command, INSERT_PAYMENT, async (payload: InsertPaymentCmd): Promise<InsertPaymentCmdRes> => {
    const payment = new Payment(payload);
    await payment.save();
    return <InsertPaymentCmdRes> payment.toObject();
});

registerTask(TaskType.command, INSERT_EVENT, async (payload: InsertEventCmd, metadata: Metadata): Promise<InsertEventCmdRes> => {
    const conditions = {
        coin: payload.coin,
        address: payload.address,
        challenge: metadata.challenge
    };

    const update: any = {
        $push: {events: omit(payload, 'coin', 'address')}
    };

    const payment = await Payment.findOneAndUpdate(conditions, update, {new: true}).exec();
    if (!payment) {
        throw new PaymentNotFound();
    }
    return <InsertEventCmdRes> payment.toObject();
});
