import {startApp} from '@chainizer/support-app';
import {logger} from '@chainizer/support-winston';
import '@chainizer/support-cnq';
import '@chainizer/support-mongoose';
import './commands';
import './queries';

startApp().catch(error => logger().error('App unable to start!', error));
